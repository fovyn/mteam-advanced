import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SecurityModule} from './modules/security/security.module';
import {HttpClientModule} from '@angular/common/http';
import {CustomFormModule} from './modules/shared/modules/custom-form/custom-form.module';
import {SubjectModule} from './modules/subject/subject.module';
import {TabsModule} from './modules/shared/modules/tabs/tabs.module';
import { Tab1Component } from './components/tab-page/tab1/tab1.component';
import { TabPageComponent } from './components/tab-page/tab-page.component';
import { Tab2Component } from './components/tab-page/tab2/tab2.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    Tab1Component,
    TabPageComponent,
    Tab2Component
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SecurityModule,
    HttpClientModule,
    CustomFormModule,
    TabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
