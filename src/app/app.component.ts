import { Component } from '@angular/core';
import {SessionService} from './modules/security/services/session.service';
import {AuthService} from './modules/security/services/auth.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import jwtDecode from 'jwt-decode';
import {FormArray, FormControl} from '@angular/forms';
import {MChipData} from './modules/shared/modules/custom-form/directives/m-chips.directive';
import {NavService} from './services/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mteam-formation-advanced';

  get user$(): Observable<any> { return this.$session.token$.pipe(map(token => token ? jwtDecode(token) : null))}

  constructor(private $session: SessionService, private $auth: AuthService, private $nav: NavService) {
    $session.init();
    $session.subscribe(token => $nav.goToHome(token));
  }

  signOutAction() {
    this.$session.close();
  }
}
