import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TabsComponent} from './modules/shared/modules/tabs/components/tabs/tabs.component';
import {Tab1Component} from './components/tab-page/tab1/tab1.component';
import {TabPageComponent} from './components/tab-page/tab-page.component';
import {Tab2Component} from './components/tab-page/tab2/tab2.component';

const routes: Routes = [
  { path: 'tabs', component: TabPageComponent, children: [
      { path: 'tab1', component: Tab1Component, data: {animation: 'tab1'} },
      { path: 'tab2', component: Tab2Component, data: {animation: 'tab2'} },
      { path: '', redirectTo: 'tab1', pathMatch: 'full'}
    ]
  },
  { path: 'subjects', loadChildren: () => import('./modules/subject/subject.module').then(m => m.SubjectModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
