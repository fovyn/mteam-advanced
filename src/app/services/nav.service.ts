import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../modules/security/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor(private $router: Router, private $auth: AuthService) { }

  goToLogin(): Promise<boolean> { return this.$router.navigate(['/sign-in']); }
  goToSubjectList(): Promise<boolean> { return this.$router.navigate(['/subjects', 'list']); }

  goToRelavive(relativeLink: string): Promise<boolean> { return this.$router.navigateByUrl(relativeLink); }

  goToHome(token: any) {
    const decoded = this.$auth.decode(token);

    if (decoded?.home) {
      this.$router.navigateByUrl(decoded.home).then(_ => console.log("Home redirection => "+ decoded.home));
    }
  }
}
