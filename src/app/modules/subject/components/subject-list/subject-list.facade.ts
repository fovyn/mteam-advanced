import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {concat, Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class SubjectListFacade {
  private API = `${environment.api_url}/subjects`;

  constructor(private $http: HttpClient) {
  }

  load(): Observable<any[]> {
    return this.$http.get<any[]>(`${this.API}?_expand=user&_expand=keywords`);
  }

  removeById(id: number) {
    return concat(this.$http.delete<any[]>(`${this.API}/${id}`), this.load());
  }
}
