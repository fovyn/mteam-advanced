import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavService} from '../../../../services/nav.service';
import {SubjectListFacade} from './subject-list.facade';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {first} from 'rxjs/operators';
import {Subject, SubjectMapper} from '../../models/subject.model';
import {AuthService} from '../../../security/services/auth.service';

@Component({
  selector: 'subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.scss'],
  providers: [SubjectListFacade]
})
export class SubjectListComponent implements OnInit, OnDestroy {
  private _subjects$: BehaviorSubject<Subject[]> = new BehaviorSubject<Subject[]>([]);
  get subjects$(): Observable<any[]> { return this._subjects$.asObservable(); }

  hasRoles(...roles: string[]): boolean { return this.$auth.hasRole(roles); }

  constructor(private $slf: SubjectListFacade, public $auth: AuthService) {
    this._subjects$.subscribe(test => console.log(test));
  }

  ngOnInit(): void {
    this.$slf.load().pipe(first()).subscribe(subjects => this._subjects$.next(SubjectMapper.fromDTOs(subjects)));
  }
  ngOnDestroy() {
  }

  remove(id: number) {
    this.$slf.removeById(id).subscribe(data => {
      if (Array.isArray(data)) {
        this._subjects$.next(data);
      }
    });
  }
}
