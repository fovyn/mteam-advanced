import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SubjectService} from '../../services/subject.service';

@Component({
  selector: 'subject-details',
  templateUrl: './subject-details.component.html',
  styleUrls: ['./subject-details.component.scss']
})
export class SubjectDetailsComponent implements OnInit {

  constructor(private $activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.$activatedRoute.data.subscribe(({subject}) => console.log(subject));
  }

}
