export class Subject {
  private _id: number;
  get id(): number { return this._id; }
  set id(value: number) { this._id = value; }

  private _title: string;
  get title(): string {return this._title;}
  set title(value: string) { this._title = value; }

  private _userId: number;
  get userId(): number { return this._userId; }
  set userId(value: number) { this._userId = value; }

  private _user?: any;
  get user(): any { return this._user; }
  set user(value: any) { this._user = value; }

  private _keywordsId: number[];
  get keywordsId(): number[] { return this._keywordsId; }
  set keywordsId(value: number[]) { this._keywordsId = value; }

  private _keywords: any[];
  get keywords(): any[] { return this._keywords; }
  set keywords(value: any[]) { this._keywords = value; }
  get keywordsStr(): string { return this.keywords?.map(it => it.tag)?.join(", "); }

  private _createAt: Date | string;
  get createAt(): Date | string { return this._createAt; }
  set createAt(value: Date | string) { this._createAt = value; }

  private _updateAt: Date | string;
  get updateAt(): Date | string { return this._updateAt; }
  set updateAt(value: Date | string) { this._updateAt = value; }

  private _isActive: boolean;
  get isActive(): boolean { return this._isActive; }
  set isActive(value: boolean) { this._isActive = value; }

  constructor(obj?: Partial<Subject>) {
    if (obj) {
      this.id = obj?.id;
      this.title = obj?.title;
      this.userId = obj?.userId;
      this.user = obj?.user;
      this.keywordsId = obj?.keywordsId;
      this.keywords = obj?.keywords;
      this.createAt = obj?.createAt;
      this.updateAt = obj?.updateAt;
      this.isActive = obj?.isActive ?? false;
    }
  }

}

export class SubjectMapper {
  static fromDTO(dto: any): Subject {
    return new Subject(dto);
  }

  static fromDTOs(dtos: any[]): Subject[] {
    const subjects = [];

    for(let dto of dtos) {
      subjects.push(this.fromDTO(dto));
    }

    return subjects;
  }
}
