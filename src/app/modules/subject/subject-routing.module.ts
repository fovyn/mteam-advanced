import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { UserGuard } from '../security/guards/user.guard';
import { RoleGuard } from '../security/guards/role.guard';
import {SubjectDetailsComponent} from './components/subject-details/subject-details.component';
import {SubjectDetailResolver} from './resolvers/subject-detail.resolver';

const routes: Routes = [
  { path: '', canActivateChild: [UserGuard], children: [
      {
        path: 'list',
        component: SubjectListComponent,
        canActivate: [RoleGuard],
        data: { roles: ['ROLE_USER', 'ROLE_ADMIN'] }
      },
      {
        path: ':id',
        component: SubjectDetailsComponent,
        resolve: {subject: SubjectDetailResolver}
      },
      {
        path: '', pathMatch: 'full', redirectTo: 'list'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubjectRoutingModule { }
