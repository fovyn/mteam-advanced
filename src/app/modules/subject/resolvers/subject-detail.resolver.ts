import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {Subject} from '../models/subject.model';
import {SubjectService} from '../services/subject.service';

@Injectable()
export class SubjectDetailResolver implements Resolve<Subject> {
  private _subjects: Subject[] = [];

  constructor(private $ss: SubjectService) {
    this.$ss.subscribe(subjects => this._subjects = subjects);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Subject> {
    const {id} = route.params;

    const finded = this._subjects.find(it => it.id == id);
    if (finded) {
      return of(finded);
    }

    return this.$ss.getOneById(id);
  }
}
