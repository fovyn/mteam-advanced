import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Subject, SubjectMapper} from '../models/subject.model';
import {environment} from '../../../../environments/environment';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private _subjects = new BehaviorSubject<Subject[]>([]);

  constructor(private $http: HttpClient) { }

  subscribe(obs: (subjects: Subject[]) => void): Subscription {
    return this._subjects.subscribe(obs);
  }

  getAll(): Observable<Subject[]> {
    return this.$http
      .get<any[]>(environment.api_url+ "/subjects")
      .pipe(map(a => SubjectMapper.fromDTOs(a)));
  }

  getOneById(id: string): Observable<Subject> {
    console.log("id");
    return this.$http
      .get(environment.api_url+ "/subjects/"+ id)
      .pipe(
        map(it => SubjectMapper.fromDTO(it)),
        tap(it => {
          const value = this._subjects.value;
          value.push(it);
          this._subjects.next(value);
        })
      );
  }
}
