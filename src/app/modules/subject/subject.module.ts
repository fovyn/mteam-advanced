import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubjectRoutingModule } from './subject-routing.module';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectDetailsComponent } from './components/subject-details/subject-details.component';
import {SubjectDetailResolver} from './resolvers/subject-detail.resolver';


@NgModule({
  declarations: [
    SubjectListComponent,
    SubjectDetailsComponent
  ],
  imports: [
    CommonModule,
    SubjectRoutingModule
  ],
  providers: [
    SubjectDetailResolver
  ]
})
export class SubjectModule { }
