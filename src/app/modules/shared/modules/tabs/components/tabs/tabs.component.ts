import {AfterViewInit, Component, ContentChild, OnInit} from '@angular/core';
import {TabBarComponent} from '../tab-bar/tab-bar.component';
import {RouterOutlet} from '@angular/router';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterViewInit {
  @ContentChild(TabBarComponent) tabBar: TabBarComponent;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {

  }

  setAnimation(outlet: RouterOutlet) {
    console.log(outlet);
    return outlet && outlet.activatedRoute && outlet.activatedRouteData.animation;
  }
}
