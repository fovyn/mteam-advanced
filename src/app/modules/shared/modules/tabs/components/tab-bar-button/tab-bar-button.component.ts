import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'tab-bar-button',
  templateUrl: './tab-bar-button.component.html',
  styleUrls: ['./tab-bar-button.component.scss']
})
export class TabBarButtonComponent implements OnInit {
  @Input('tab') tab: string;
  @Input('label') label: string;

  constructor() { }

  ngOnInit(): void {
  }

}
