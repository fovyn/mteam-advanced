import {AfterViewInit, Component, ContentChildren, OnInit, QueryList, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {TabBarButtonComponent} from '../tab-bar-button/tab-bar-button.component';

@Component({
  selector: 'tab-bar',
  templateUrl: './tab-bar.component.html',
  styleUrls: ['./tab-bar.component.scss']
})
export class TabBarComponent implements OnInit, AfterViewInit {
  @ContentChildren(TabBarButtonComponent) buttons: QueryList<TabBarButtonComponent>;
  @ViewChild("templateRef", {read: TemplateRef}) tRef: TemplateRef<{ routerLink, label }>

  constructor(private $vcr: ViewContainerRef) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    console.log(this.buttons);
    this.buttons.forEach(btn =>
      this.$vcr.createEmbeddedView(this.tRef, {routerLink: btn.tab, label: btn.label})
    );
  }

}
