import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabBarComponent } from './components/tab-bar/tab-bar.component';
import {RouterModule} from '@angular/router';
import { TabBarButtonComponent } from './components/tab-bar-button/tab-bar-button.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';



@NgModule({
  declarations: [
    TabsComponent,
    TabBarComponent,
    TabBarButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterModule
  ],
  exports: [
    TabsComponent,
    TabBarComponent,
    TabBarButtonComponent
  ]
})
export class TabsModule { }
