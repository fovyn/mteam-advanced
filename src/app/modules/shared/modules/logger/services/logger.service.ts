import {Injectable} from '@angular/core';
import {LogLevel} from '../models/log-level.enum';
import {LogEntry} from '../models/log-entry';

@Injectable()
export class LoggerService {
  private _styles = {
    debug: 'color: #ff0000'
  };

  constructor(
    private $level: LogLevel, //Level de log minimum
    private $logWithDate: boolean = false
  ) {
  }

  debug(src: string, msg: any, params: any[] = []) {
    this._writeLog(src, msg, LogLevel.Debug, params, this._styles.debug);
  }

  private _writeLog(src: string, msg: any, level: LogLevel, params: any[], color?: string) {
    if (this._shouldLog(level)) {
      const entry = new LogEntry();
      entry.src = src;
      entry.message = msg;
      entry.level = level;
      entry.extraInfo = params;
      entry.logWithDate = this.$logWithDate;

      switch (level) {
        case LogLevel.All:
        case LogLevel.Debug:
        case LogLevel.Info:
        case LogLevel.Warn:
          console.log(`%c${entry.buildLogString()} =>`, color, entry.message);
          break;
        default:
          console.error(`%c${entry.buildLogString()} =>`, color, entry.message);
          break;
      }
    }
  }

  private _shouldLog(level: LogLevel): boolean {
    return (level >= this.$level && level !== LogLevel.Off) || this.$level === LogLevel.All;
  }
}
