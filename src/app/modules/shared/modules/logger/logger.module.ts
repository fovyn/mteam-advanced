import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LogLevel} from './models/log-level.enum';
import {LoggerService} from './services/logger.service';


export const LOGGER_LVL = 'logger_lvl';
export const LOGGER_WITH_DATE = 'logger_withDate';
export interface LoggerOptions {
  lvl: LogLevel,
  withDate: boolean
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class LoggerModule {
  static forRoot(options: Partial<LoggerOptions> = {lvl: LogLevel.Info, withDate: true}): ModuleWithProviders<LoggerModule> {
    return {
      ngModule: LoggerModule,
      providers: [
        { provide: LOGGER_LVL, useValue: options.lvl },
        { provide: LOGGER_WITH_DATE, useValue: options.withDate },
        { provide: LoggerService, useFactory: () => new LoggerService(options.lvl, options.withDate) }
      ]
    }
  }
}
