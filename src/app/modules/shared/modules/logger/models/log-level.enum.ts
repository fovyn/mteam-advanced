export enum LogLevel {
  Debug,
  Info,
  Warn,
  Error,
  Fatal,
  Wtf,
  Off,
  All
}
