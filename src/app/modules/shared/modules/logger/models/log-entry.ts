import {LogLevel} from './log-level.enum';

export class LogEntry {
  src: string;
  logWithDate: boolean;
  level: LogLevel;
  message: string;
  extraInfo: any[];

  buildLogString(): string {
    let ret: string = "";

    if (this.logWithDate) {
      ret = new Date().toISOString();
    }

    ret += `Src: ${this.src}`
    ret += ` - Type: ${LogLevel[this.level]}`;
    if (this.extraInfo.length) {
      ret += ` - Extra Info: ` + this.formatParams(this.extraInfo);
    }

    return ret;
  }

  private formatParams(params: any[]): string {
    let ret: string = params.join(",");

    // Is there at least one object in the array?
    if (params.some(p => typeof p == "object")) {
      ret = "";

      // Build comma-delimited string
      for (let item of params) {
        ret += JSON.stringify(item) + ",";
      }
    }

    return ret;
  }
}
