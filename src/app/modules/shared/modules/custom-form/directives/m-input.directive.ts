import {AfterViewInit, Directive, ElementRef} from '@angular/core';
import * as M from 'materialize-css';

@Directive({
  selector: '[mInput]'
})
export class MInputDirective implements AfterViewInit {

  get el(): HTMLInputElement { return this.elRef.nativeElement; }

  constructor(private elRef: ElementRef<HTMLInputElement>) { }

  ngAfterViewInit(): void {
    M.updateTextFields();
  }
}
