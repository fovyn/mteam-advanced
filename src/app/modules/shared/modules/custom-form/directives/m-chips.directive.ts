import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {AbstractControl, FormArray} from '@angular/forms';
import {FormChipsParameterError, ParameterErrorType} from '../errors/form-chips-parameter.error';

@Directive({
  selector: '[mChips]'
})
export class MChipsDirective implements AfterViewInit, OnDestroy {
  @Output('update') updateEvent = new EventEmitter<MChipDatas>();

  @Input('tags') tags: Observable<M.AutocompleteData> | M.AutocompleteData = null;
  @Input('options') options: Partial<M.ChipsOptions> = {};
  @Input('formArray') formArray: FormArray = null;
  @Input('faFactory') faFactory: (it: MChipData) => AbstractControl;
  @Input('chipLimit') chipLimit: number = Infinity;

  private _mChips: M.Chips;
  get mChips(): M.Chips { return this._mChips; }

  get el(): HTMLDivElement { return this.$elRef.nativeElement; }

  constructor(private $elRef: ElementRef<HTMLDivElement>) {}

  ngAfterViewInit(): void {
    this._initFormArray();

    if (this.tags instanceof Observable) {
      this.tags.subscribe(tags => this._createMChips(tags));
    } else {
      this._createMChips(this.tags);
    }
    this._initChipClasses();
  }

  private _initFormArray() {
    if (this.formArray && this.faFactory) {
      this.updateEvent
        .subscribe(
          (data: MChipDatas) => data.forEach( it => this.formArray.push(this.faFactory(it)) )
        );
    }
    else if (this.formArray && !this.faFactory) { throw new FormChipsParameterError(ParameterErrorType.FormArray_Factory); }
    else if (!this.formArray && this.faFactory) { throw new FormChipsParameterError(ParameterErrorType.Factory_FormArray); }
  }

  ngOnDestroy(): void {
    this._mChips.destroy();
  }

  private _initChipClasses(): void {
    this.el.classList.add("chips", "chips-autocomplete");
  }

  private _createMChips(tags?: M.AutocompleteData) {
    if (tags) {
      this._mChips = M.Chips.init(this.el, {
        ...this.options,
        autocompleteOptions: {data: tags},
        limit: this.chipLimit,
        onChipAdd: (els) => this.updateEvent.emit(this._mChips.chipsData),
        onChipDelete: (els) => this.updateEvent.emit(this._mChips.chipsData)
      });
    } else {
      this._mChips = M.Chips.init(this.el, {
        ...this.options,
        limit: this.chipLimit,
        onChipAdd: (els) => this.updateEvent.emit(this._mChips.chipsData),
        onChipDelete: (els) => this.updateEvent.emit(this._mChips.chipsData)
      });
    }
  }
}

export type MChipDatas = Array<MChipData>;
export type MChipData = {tag: string, image?: string};
