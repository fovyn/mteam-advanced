import {Component, Input, OnInit, Optional} from '@angular/core';
import {AbstractControl, ControlValueAccessor, NgControl} from '@angular/forms';
import { v4 as Uuid } from 'uuid';

@Component({
  selector: 'input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit, ControlValueAccessor {
  @Input('type') type: 'text' | 'number' | 'password' = 'text';
  @Input("disabled") disabled: boolean = false;
  @Input("label") label: string = "";

  id = Uuid();
  value: any;

  get control(): AbstractControl { return this.$ngControl.control; }

  onChange = (v: any) => {};
  onTouched = () => {};

  constructor(@Optional() private $ngControl?: NgControl) {
    if (this.$ngControl) {
      this.$ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
    this.onChange(obj);
  }


}
