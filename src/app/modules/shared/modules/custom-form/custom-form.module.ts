import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MInputDirective } from './directives/m-input.directive';
import { MChipsDirective } from './directives/m-chips.directive';
import { InputFieldComponent } from './components/input-field/input-field.component';
import {FormsModule} from '@angular/forms';



@NgModule({
    declarations: [
        MInputDirective,
        MChipsDirective,
        InputFieldComponent
    ],
  exports: [
    MInputDirective,
    MChipsDirective,
    InputFieldComponent
  ],
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class CustomFormModule { }
