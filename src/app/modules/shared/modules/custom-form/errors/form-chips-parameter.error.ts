export enum ParameterErrorType {
  FormArray_Factory,
  Factory_FormArray
}
export class FormChipsParameterError extends Error {

  constructor(type: ParameterErrorType) {
    super();

    switch (type) {
      case ParameterErrorType.Factory_FormArray: this.message = 'Factory provide but not FormArray'; break;
      case ParameterErrorType.FormArray_Factory: this.message = 'FormArray provide but not Factory'; break;
    }
  }
}
