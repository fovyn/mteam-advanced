import {AbstractControl, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export class FormValidator {
  static same(field1: string, field2: string): ValidatorFn {
    return function(group: FormGroup): ValidationErrors {
      const controlField1: AbstractControl = group.get(field1);
      const controlField2: AbstractControl = group.get(field2);

      if (controlField1.value === controlField2.value) {
        return null;
      }

      return {'same': `Le champ ${field1} n'est pas égale au champ ${field2}`};

      return {'same': ''}
    }
  }
}
