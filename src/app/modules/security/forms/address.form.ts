import {AbstractControl, FormControl, Validators} from '@angular/forms';

export const F_RegisterAddress: {[key: string]: AbstractControl} = {
  // street: new FormControl(null, [Validators.required]),
  bte: new FormControl(null, [Validators.required]),
  city: new FormControl('Bruxelles', [Validators.required]),
  country: new FormControl('Belgique', [])
}
