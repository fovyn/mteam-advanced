import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {F_RegisterAddress} from './address.form';

export const F_SignIn: {[key: string]: AbstractControl} = {
  username: new FormControl(null, [Validators.required]),
  password: new FormControl(null, [
    Validators.required,
    Validators.minLength(5)
  ])
}

export const F_Register: {[key: string]: AbstractControl} = {
  username: new FormControl(null, [Validators.required]),
  password: new FormControl(null, [Validators.required, Validators.minLength(5)]),
  verifPassword: new FormControl(null, [Validators.required, Validators.minLength(5)]),
  address: new FormGroup(F_RegisterAddress)
}


