import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityRoutingModule } from './security-routing.module';
import { SignInComponent } from './components/sign-in/sign-in.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CustomFormModule} from '../shared/modules/custom-form/custom-form.module';
import { RegisterComponent } from './components/register/register.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {Back2Interceptor} from './interceptors/back2.interceptor';


@NgModule({
  declarations: [
    SignInComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    SecurityRoutingModule,
    ReactiveFormsModule,
    CustomFormModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: Back2Interceptor, multi: true },
  ]
})
export class SecurityModule { }
