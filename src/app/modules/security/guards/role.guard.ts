import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {SessionService} from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, OnDestroy {
  private _session$$: Subscription;
  private _token: string = null;

  constructor(private $auth: AuthService, private $session: SessionService, private $router: Router) {
    this._session$$ = $session.subscribe(token => this._token = token);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this._token) { return false; }

    const {roles} = route.data;

    const decoded = this.$auth.decode(this._token);

    const finded = decoded.roles.find(r => roles.find(it => it == r) != null);

    return finded != null;
  }

  ngOnDestroy() {
    this._session$$.unsubscribe();
  }
}
