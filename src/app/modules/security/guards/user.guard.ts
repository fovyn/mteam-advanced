import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {SessionService} from '../services/session.service';

/// Suis-je un utilisateur connecté
@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate, CanActivateChild, OnDestroy {
  private _sessionSubscription: Subscription;
  private _token: string = null;

  constructor(private $auth: AuthService, private $session: SessionService, private $router: Router) {
    this._sessionSubscription = $session.subscribe(token => this._token = token);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this._token) {
      this.$router.navigate(['/sign-in']).then(_ => console.log("NAVIGATE"));
      return false;
    }
    return true;
  }

  ngOnDestroy() {
    this._sessionSubscription.unsubscribe();
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(childRoute, state);
  }
}
