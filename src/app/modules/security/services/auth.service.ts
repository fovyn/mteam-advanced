import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SessionService} from './session.service';
import {concat, Observable, Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import jwtDecode from 'jwt-decode';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL = `${environment.api_url}/auth`
  private _token: string = null;

  constructor(private $http: HttpClient, private $session: SessionService) {
    $session.subscribe(token => this._token = token);
  }

  login(username: string, password: string): Observable<any> {
    const params = new HttpParams()
      .set("userId", "");
    const loginSubject = new Subject<any>();

    this.$http.get('', {params});

    this.$http
      .post<any>(`${this.API_URL}/login`, {username, password})
      .pipe(map(data => data.token))
      .subscribe(token => {
        this.$session.open(token);
        loginSubject.next(token);
      })


    return loginSubject.asObservable();
  }

  decode(token: string): any {
    return jwtDecode(token);
  }

  hasRole(rs: string[]): boolean {
    const {roles} = this.decode(this._token);

    return rs.find(r => roles.find(it => it == r) != null) != null;
  }
}
