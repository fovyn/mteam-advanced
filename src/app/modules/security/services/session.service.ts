import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, PartialObserver, Subscription} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _token$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  get token$(): Observable<string> { return this._token$.asObservable(); }

  constructor() { }

  subscribe(obs: (it: any) => void): Subscription {
    return this._token$.subscribe(obs);
  }

  open(token: string) {
    localStorage.setItem(environment.storage.token, token);
    this._token$.next(token);
  }

  close() {
    localStorage.removeItem(environment.storage.token);
    this._token$.next(null);
  }

  init() {
    const token = localStorage.getItem(environment.storage.token);

    if (token) { this._token$.next(token); }
  }
}
