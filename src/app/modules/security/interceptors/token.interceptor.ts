import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {SessionService} from '../services/session.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private _token: string = null;

  constructor(private $session: SessionService) {
    $session.subscribe(token => this._token = token);
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //Si je ne gère le cas je renvoi au prochain interceptor
    if (!request.url.match(/.*(\:3001).*/ig)) { return next.handle(request); }

    if (this._token) {
      const clone = request.clone({
        headers: new HttpHeaders({Authorization: `Bearer ${this._token}`})
      });
      return next.handle(clone).pipe(catchError((err, event: Observable<HttpEvent<any>>) => {
        switch (err.status) {
          case 404: M.toast({html: 'ERREUR 404 => Page not found'}); break;
          default: M.toast({html:"ERROR"}); break;
        }

        return throwError(err);
      }));
    }
    return next.handle(request).pipe(catchError((err, event: Observable<HttpEvent<any>>) => {
      M.toast({html: "ERROR"});

      return throwError(err);
    }));
  }
}
