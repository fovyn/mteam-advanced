import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {F_SignIn} from '../../forms/user.form';
import {Router} from '@angular/router';
import {NavService} from '../../../../services/nav.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, AfterViewInit {
  fg: FormGroup

  constructor(private $auth: AuthService, private $formBuilder: FormBuilder, private $nav: NavService) { }

  ngOnInit(): void {
    this.fg = this.$formBuilder.group(F_SignIn);
    this.fg.get('username').disable();
  }

  ngAfterViewInit(): void {
    // M.AutoInit();
  }


  handleSubmitAction() {
    if (!this.fg.valid) { return; }
    const { username, password } = this.fg.value;
    this.$auth
      .login(username, password)
      .subscribe(token => this.$nav.goToSubjectList());
  }
}
