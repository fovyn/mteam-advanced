import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../services/auth.service';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class RegisterFacade {
  private API = `${environment.api_url}/users`;

  constructor(private $http: HttpClient, private $auth: AuthService) {

  }

  register(registerDto: any): Observable<any> {
    const subject = new Subject();
    this.$http
      .post<any>(this.API, registerDto)
      .subscribe(user => {
        this.$auth.login(user.username, user.password);
        subject.next(user);
      });

    return subject.asObservable();
  }
}
