import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {F_Register} from '../../forms/user.form';
import {RegisterFacade} from './register.facade';
import {FormValidator} from '../../forms/form.validator';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [RegisterFacade]
})
export class RegisterComponent implements OnInit {
  fg: FormGroup

  get authData(): FormGroup { return this.fg.get('authData') as FormGroup; }
  get address(): FormGroup { return this.fg.get('address') as FormGroup; }

  constructor(private $formBuilder: FormBuilder, private $rf: RegisterFacade) { }

  ngOnInit(): void {
    this.fg = this.$formBuilder
      .group(F_Register, {validators: [FormValidator.same('password', 'verifPassword')]});
  }

  handleSubmitAction() {
    if (this.fg.valid) {
      const {authData, address} = this.fg.value;

      this.$rf
        .register({username: authData.username, password: authData.password, isActive: true, address})
        .subscribe(newUser => console.log(newUser));
    }
  }
}
