import { Component, OnInit } from '@angular/core';
import {slideAnimation} from '../../modules/shared/modules/tabs/slide.animation';

@Component({
  selector: 'tab-page',
  templateUrl: './tab-page.component.html',
  styleUrls: ['./tab-page.component.scss'],
  animations: [
    slideAnimation
  ]
})
export class TabPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
